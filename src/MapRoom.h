#ifndef _MAP_ROOM_H_
#define _MAP_ROOM_H_

#include <iostream>

#include "MapGeneric.h";
#include "Player.h"
#include "Resources.h";
#include "Physics.h"
#include "Talking.h"
#include "GlobalConfig.h"

class MapRoom : public MapGeneric {
public:
  MapRoom();
  ~MapRoom();

  void init(Player* player) override;
  void update(float deltatime, Player* player) override;
  void draw(Player* player) override;
  void off(Player* player) override;

  bool actionableDoor = false;
  Rect<int32_t> doorRect;

  std::vector<uint32_t> bodies;

  std::string animSofa01 = "data/Textures/sofa01.json";
  std::string animSofa02 = "data/Textures/sofa02.json";

  bool aresit = false;
  int32_t aretalking = 0;
  Rect<int32_t> sofa01Rect;
  Rect<int32_t> sofa02Rect;

  Talking talking;
};

#endif
