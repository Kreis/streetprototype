#include "MapMenu.h"

MapMenu::MapMenu() {}
MapMenu::~MapMenu() {}

void MapMenu::init(Player* player) {

  name = "data/Map/Menu.json";
  nextMapName = name;

  std::cout << "init map " << name << std::endl;
  
  Resources::get()->loadMap(name);
  Map* map = Resources::get()->getMap(name);

  SpriteBatch::get()->fixVecVertex(
    map->tilesetTextureName,
    map->getVecVertex());

  
  for (int32_t i = 0; i < map->objectsName.size(); i++) {
    std::cout << "iterate object "
      << map->objectsName[ i ] << std::endl;

    if (map->objectsName[ i ] == "startButton") {
      startRect = map->objectsRect[ i ];
    }

    if (map->objectsName[ i ] == "exitButton") {
      exitRect = map->objectsRect[ i ];
    }
  }

  Camera::get()->bounds.x = 0;
  Camera::get()->bounds.y = 200;

  // exit button
  exitName = "data/Textures/exit.png";
  exitOverName = "data/Textures/exitOver.png";
  Resources::get()->loadTexture(exitName);
  Resources::get()->loadTexture(exitOverName);

  sf::Texture* exitTexture = Resources::get()->getTexture(exitName);
  Rect<int32_t> genericRect;
  genericRect.x = 0;
  genericRect.y = 0;
  genericRect.w = exitTexture->getSize().x;
  genericRect.h = exitTexture->getSize().y;

  exitSprite.init(exitName, genericRect);
  exitSprite.bounds.x = exitRect.x;
  exitSprite.bounds.y = exitRect.y;

  // start button
  startName = "data/Textures/start.png";
  startOverName = "data/Textures/startOver.png";
  Resources::get()->loadTexture(startName);
  Resources::get()->loadTexture(startOverName);

  startSprite.init(startName, genericRect);
  startSprite.bounds.x = startRect.x;
  startSprite.bounds.y = startRect.y;

  selectedButton = 1;

  // sound
  Resources::get()->loadSound("data/Sound/moveMenu.wav");
}

void MapMenu::update(float deltatime, Player* player) {

  if (selectedButton == 1) {

    startSprite.textureName = startOverName;
    exitSprite.textureName = exitName;

  } else {

    startSprite.textureName = startName;
    exitSprite.textureName = exitOverName;

  }
  
  if (InputState::get()->isTyped(KeyButton::Down)) {
    selectedButton = 2;
    
    Resources::get()->getSound("data/Sound/moveMenu.wav")->play();
  }
  if (InputState::get()->isTyped(KeyButton::Up)) {
    selectedButton = 1;
    Resources::get()->getSound("data/Sound/moveMenu.wav")->play();
  }

  if (InputState::get()->isTyped(KeyButton::Enter)) {

    std::cout << "hit enter in menu" << std::endl;
    if (selectedButton == 2) {
      GlobalConfig::get()->exitGame = true;
    }
    
    if (selectedButton == 1) {
      nextMapName = "data/Map/Street.json";
    }
  }

}

void MapMenu::draw(Player* player) {
  SpriteBatch::get()->drawSprite(&startSprite);
  SpriteBatch::get()->drawSprite(&exitSprite);
}

void MapMenu::off(Player* player) {
  SpriteBatch::get()->clearVecVertex("data/Textures/Tiles.png");
}










