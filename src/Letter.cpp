#include "Letter.h"

Letter::Letter(
    char letter,
    int32_t x,
    int32_t y,
    int32_t width,
    int32_t height,
    int32_t xoffset,
    int32_t yoffset,
    int32_t xadvance,
    std::string texture_name) {

	this->letter = letter;
	this->texture_rect.x = x;
	this->texture_rect.y = y;
	this->texture_rect.w = width;
	this->texture_rect.h = height;
	this->xoffset = xoffset;
	this->yoffset = yoffset;
	this->xadvance = xadvance;
	this->texture_name = texture_name;
}
