#include "Animator.h"

Animator::Animator() {}
Animator::~Animator() {}

void Animator::runAnimation(std::string animationName) {
  if (animationName == currentAnimation) {
    return;
  }

  if (animations.find(animationName) == animations.end()) {
    std::cout << "ERROR: not found animation " << animationName
      << " in Animator" << std::endl;
    return;
  }

  currentAnimation = animationName;
  frameId = 0;
}

void Animator::update(float deltatime) {
  if (currentAnimation.length() == 0) {
    std::cout << "ERROR: no current animation set" << std::endl;
    return;
  }

  currentTime += deltatime;
  if (currentTime >= delays[ currentAnimation ][ frameId ]) {
    currentTime = 0;
    frameId = frameId + 1 < delays[ currentAnimation ].size()
      ? frameId + 1 : 0;
  }
}

void Animator::draw() {
  if (currentAnimation.length() == 0) {
    std::cout << "ERROR: no current animation set" << std::endl;
    return;
  }

  Rect<int32_t>& rect = animations[ currentAnimation ][ frameId ];

  sprite.textureName = textureName;
  sprite.bounds = bounds;
  sprite.textureRect = rect;
  if (animationsColor[ currentAnimation ].size() > 0) {
    sprite.color = animationsColor[ currentAnimation ][ frameId ];
  }
  SpriteBatch::get()->drawSprite(&sprite);
}

void Animator::setHorizontalSwitch(bool v) {
  sprite.horizontalSwitch = v;
}

void Animator::setVerticalSwitch(bool v) {
  sprite.verticalSwitch = v;
}


