#include "Player.h"

Player::Player() {}
Player::~Player() {}

void Player::init() {

  std::cout << "Player init" << std::endl;

  Resources::get()->loadAnimator("data/Textures/player.json");
  Animator* animator = Resources::get()->getAnimator("data/Textures/player.json");
  animator->runAnimation("idle");
  
  bodyId = Physics::get()->generateBody(
    animator->bounds.x,
    animator->bounds.y,
    animator->bounds.w,
    animator->bounds.h,
    /*isDynamic=*/true);
  
  jumping = false;
}

void Player::update(float deltatime) {

  Animator* animator = Resources::get()->getAnimator("data/Textures/player.json");

  animator->update(deltatime);

  Body* body = Physics::get()->getBody(bodyId);

  bool right = InputState::get()->isPressed(KeyButton::Right);
  bool left = InputState::get()->isPressed(KeyButton::Left);
  if (right != left) {
    animator->runAnimation("walking");
    if (right) {
      body->move(400.0 * deltatime, 0);
      animator->setHorizontalSwitch(false);
    } else {
      body->move(-400.0 * deltatime, 0);
      animator->setHorizontalSwitch(true);
    }
  } else {
    animator->runAnimation("idle");
  }

  // gravity
  //body->move(0, 200.0 * deltatime);

  // jump
  if (InputState::get()->isTyped(KeyButton::Space) &&
    ! jumping) {

    std::cout << "jumping" << std::endl;
    jumping = true;
    parabola.init(
    body,
    /*time to peak=*/0.3,
    /*height=*/96,
    /*current time=*/0,
    TypeParabola::VERTICAL_INVERTED);
  }
  if (jumping) {
    parabola.update(deltatime, body);
  }
  if (body->collidingDown) {
    std::cout << "collide down" << std::endl;
    jumping = false;
  }
  animator->bounds.x = (int32_t)body->getX().get();
  animator->bounds.y = (int32_t)body->getY().get();

  //std::cout << "y: " << animator->bounds.y << std::endl;
}

void Player::draw() {

  Animator* animator = Resources::get()->getAnimator("data/Textures/player.json");

  animator->draw();
}


Rect<int32_t> Player::getBounds() {
  Body* body = Physics::get()->getBody(bodyId);
  Rect<int32_t> r;
  r.x = body->getX().get();
  r.y = body->getY().get();
  r.w = body->getWidth().get();
  r.h = body->getHeight().get();
  return r;
}

void Player::setPosition(Vec<int32_t> pos) {
  Body* body = Physics::get()->getBody(bodyId);
  body->setPosition(pos.x, pos.y);
}



