#ifndef _TALKING_H_
#define _TALKING_H_

#include <iostream>
#include <memory> 

#include "SpriteBatch.h"
#include "Resources.h"
#include "TextArea.h"

class Talking {
public:
  Talking();
  ~Talking();

  void init(Rect<int32_t> _dialogRect);
  void update(float deltatime, int32_t tempo);
  void draw();

  std::unique_ptr<TextArea> textArea;
  Rect<int32_t> dialogRect;
  Rect<int32_t> displayRect;

  std::string animDisplay01 = "data/Textures/character01.json";
  std::string animDisplay02 = "data/Textures/character02.json";

  int32_t limitTempo = 4;
  float runningTime = 0;
private:
  int32_t talkTempo = 0;
};

#endif
