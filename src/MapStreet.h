#ifndef _MAPSTREET_H_
#define _MAPSTREET_H_

#include <iostream>

#include "Map.h"
#include "Resources.h"
#include "SpriteBatch.h"
#include "Physics.h"
#include "Player.h"
#include "Camera.h"
#include "MapGeneric.h"
#include "Animator.h"
#include "GlobalConfig.h"
#include "Clouds.h"

class MapStreet : public MapGeneric{
public:
  MapStreet();
  ~MapStreet();

  void init(Player* player) override;
  void update(float deltatime, Player* player) override;
  void draw(Player* player) override;
  void off(Player* player) override;

  bool actionableDoor = false;

  // to drop physics
  std::vector<uint32_t> bodies;

  Clouds clouds;
};

#endif
