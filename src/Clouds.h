#ifndef _CLOUDS_H_
#define _CLOUDS_H_

#include <memory>
#include <SFML/Graphics.hpp>

#include "SpriteBatch.h"
#include "Resources.h"

class Clouds {
public:
  Clouds();
  ~Clouds();

  void init();
  void update(float deltatime);
  void draw();

  std::unique_ptr<Sprite> sprite;
  float time = 0;
  std::string fragmentName;
};

#endif
