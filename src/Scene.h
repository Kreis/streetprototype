#ifndef _SCENE_H_
#define _SCENE_H_

#include <iostream>
#include <functional>
#include <memory>
#include <map>

#include "Resources.h"
#include "Physics.h"
#include "Player.h"
#include "MapStreet.h"
#include "MapMenu.h"
#include "MapRoom.h"

class Scene {
public:
  Scene();
  ~Scene();

  void init();
  void update(float deltatime);
  void draw();

private:
  std::unique_ptr<Player> player;
  // Maps
  std::map<std::string, std::unique_ptr<MapGeneric> > maps;
  MapGeneric* currentMap;
};

#endif
