#include "MapRoom.h"

MapRoom::MapRoom() {}
MapRoom::~MapRoom() {}

void MapRoom::init(Player* player) {

  name = "data/Map/Room.json";
  nextMapName = name;
  
  std::cout << "init map " << name << std::endl;

  Resources::get()->loadMap(name);
  Map* map = Resources::get()->getMap(name);

  SpriteBatch::get()->fixVecVertex(
    map->tilesetTextureName,
    map->getVecVertex());

  // animations sofa
  Resources::get()->loadAnimator(animSofa01);
  Resources::get()->loadAnimator(animSofa02);

  Resources::get()->getAnimator(animSofa01)->
    runAnimation("idle");
  Resources::get()->getAnimator(animSofa02)->
    runAnimation("idle");

  Rect<int32_t> dialogRect;

  for (int32_t i = 0; i < map->objectsName.size(); i++) {
    std::cout << "iterate object "
      << map->objectsName[ i ] << std::endl;

    if (map->objectsName[ i ] == "start_spawn") {
      Vec<int32_t> v;
      v.x = map->objectsRect[ i ].x;
      v.y = map->objectsRect[ i ].y;
      player->setPosition(v);
    } 

    if (map->objectsName[ i ] == "physics") {
      std::cout << "is physics" << std::endl;
      int32_t ptmp = Physics::get()->generateBody(
        map->objectsRect[ i ].x,
        map->objectsRect[ i ].y,
        map->objectsRect[ i ].w,
        map->objectsRect[ i ].h);
      bodies.push_back(ptmp);
    }
    
    if (map->objectsName[ i ] == "sofa01") {
      Resources::get()->getAnimator(animSofa01)->bounds.x =
        map->objectsRect[ i ].x;
      Resources::get()->getAnimator(animSofa01)->bounds.y =
        map->objectsRect[ i ].y;
      sofa01Rect = map->objectsRect[ i ];
    }

    if (map->objectsName[ i ] == "sofa02") {
      Resources::get()->getAnimator(animSofa02)->bounds.x =
        map->objectsRect[ i ].x;
      Resources::get()->getAnimator(animSofa02)->bounds.y =
        map->objectsRect[ i ].y;
      sofa02Rect = map->objectsRect[ i ];
    }

    if (map->objectsName[ i ] == "door") {
      doorRect = map->objectsRect[ i ];
    }

    if (map->objectsName[ i ] == "dialog") {
      dialogRect = map->objectsRect[ i ];
    }

  }

  Camera::get()->bounds.x = 0;
  Camera::get()->bounds.y = 0;

  // music
  Resources::get()->loadMusic("data/Sound/RoomBackground.ogg");
  sf::Music* music =
    Resources::get()->getMusic("data/Sound/RoomBackground.ogg");
  music->play();
  int32_t volume = GlobalConfig::get()->volume;
  music->setVolume((float)(volume * volume) / 100.0);
  music->setLoop(true);
  
  // dialog
  talking.init(dialogRect);

  // sounds
  Resources::get()->loadSound("data/Sound/door.wav");
}

void MapRoom::update(float deltatime, Player* player) {

  player->update(deltatime);
  Physics::get()->update();

  Rect<int32_t> prect = player->getBounds();
  actionableDoor = prect.x + prect.w > doorRect.x &&
    prect.x < doorRect.x + doorRect.w;

  float volume = GlobalConfig::get()->volume;

  // door
  if (InputState::get()->isTyped(KeyButton::Up) && actionableDoor) {
    nextMapName = "data/Map/Street.json";
    Resources::get()->getSound("data/Sound/door.wav")->setVolume(
      (float)(volume * volume) / 100.0 * 1.2);
    Resources::get()->getSound("data/Sound/door.wav")->play();
  }

  // volume
  if (InputState::get()->isTyped(KeyButton::Add)) {
    GlobalConfig::get()->volume += 10;
    if (GlobalConfig::get()->volume > 100) GlobalConfig::get()->volume = 100;
    volume = GlobalConfig::get()->volume;

    Resources::get()->getMusic("data/Sound/RoomBackground.ogg")->
      setVolume((float)(volume * volume) / 100.0);
    std::cout << "set volume as " << GlobalConfig::get()->volume << std::endl;
  }
  if (InputState::get()->isTyped(KeyButton::Subtract)) {
    GlobalConfig::get()->volume -= 10;
    if (GlobalConfig::get()->volume < 0) GlobalConfig::get()->volume = 0;
    volume = GlobalConfig::get()->volume;

    Resources::get()->getMusic("data/Sound/RoomBackground.ogg")->
      setVolume((float)(volume * volume) / 100.0);
    std::cout << "set volume as " << GlobalConfig::get()->volume << std::endl;
  }

  // check if sit
  Rect<int32_t> pb = player->getBounds();
  bool coordSit = pb.x + pb.w > sofa02Rect.x &&
    pb.x < sofa02Rect.x + sofa02Rect.w; 
  if (InputState::get()->isTyped(KeyButton::Up) && coordSit) {
    aresit = true;
  }

  // check if talking
  bool coordTalking = pb.x + pb.w > sofa01Rect.x &&
    pb.x < sofa01Rect.x + sofa01Rect.w;
  if (InputState::get()->isTyped(KeyButton::Up) && coordTalking) {
    aretalking = aretalking == talking.limitTempo ? 0 : aretalking + 1;
  }
  if (aretalking > 0) {
    talking.update(deltatime, aretalking);
  }

  // check if get up
  if (InputState::get()->isPressed(KeyButton::Left) ||
    InputState::get()->isPressed(KeyButton::Right)) {
    aresit = false;
    aretalking = 0;
  }

  // animations
  Resources::get()->getAnimator(animSofa01)->update(deltatime);
  Resources::get()->getAnimator(animSofa02)->update(deltatime);
}

void MapRoom::draw(Player* player) {

  Animator* animatorSofa01 = Resources::get()->getAnimator(animSofa01);
  Animator* animatorSofa02 = Resources::get()->getAnimator(animSofa02);

  if (aresit) {
    animatorSofa02->runAnimation("sit");
  } else {
    animatorSofa02->runAnimation("idle");
  }

  if (aretalking > 0) {
    talking.draw();
  }
  
  animatorSofa01->draw();
  animatorSofa02->draw();

  if ( ! aresit) {
    player->draw();
  }
}

void MapRoom::off(Player* player) {
  std::cout << "off map room" << std::endl;
  SpriteBatch::get()->clearVecVertex("data/Textures/Tiles.png");

  for (int32_t i : bodies) {
    Physics::get()->getBody(i)->off();
  }

  Resources::get()->getMusic("data/Sound/RoomBackground.ogg")->
    stop();

}

