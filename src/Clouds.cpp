#include "Clouds.h"

Clouds::Clouds() {}
Clouds::~Clouds() {}

void Clouds::init() {

  std::cout << "init clouds" << std::endl;

  sprite = std::make_unique<Sprite>();

  fragmentName = "data/Shaders/clouds";
  std::string textureName = "SHADERCLOUDS";

  Resources::get()->loadShader(fragmentName);
  SpriteBatch::get()->setShaderToVecVertex(
    textureName, fragmentName);
  
  Rect<int32_t> textureRect;
  textureRect.x = 0;
  textureRect.y = 0;
  textureRect.w = 1200;
  textureRect.h = 800;
  sprite->init(textureName, textureRect);


  // shader config
  sf::Shader* shader = Resources::get()->getShader(fragmentName);
  shader->setParameter("u_resolution",
    sf::Vector2f(1200, 960));
}

void Clouds::update(float deltatime) {
  time += deltatime;

  sf::Shader* shader = Resources::get()->getShader(fragmentName);
  shader->setParameter("u_time", time);
}

void Clouds::draw() {
  SpriteBatch::get()->drawSprite(sprite.get());
}

