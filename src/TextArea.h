#ifndef _TEXT_AREA_H_
#define _TEXT_AREA_H_

#include <iostream>
#include <memory>
#include <vector>
#include <assert.h>

#include "sharedtypes.h"
#include "FontConfig.h"
#include "SpriteBatch.h"

enum class TextAreaAlign {
  LEFT,
  MIDDLE
};

class TextArea {
public:
    static std::unique_ptr<TextArea> create(
      const FontConfig& font_config, const Rect<int32_t>& box);

    void setPosition(const Vec<int32_t>& position);
    void setText(
        std::wstring text,
        uint32_t color,
        TextAreaAlign align = TextAreaAlign::LEFT
    );
    bool isValid(uint32_t c) const;
    void draw();

    Rect<int32_t> getBounds();

    void clear();

private:
    TextArea();
    FontConfig font_config;
    TextAreaAlign current_align = TextAreaAlign::LEFT;
    Rect<int32_t> box;
    // TODO, create a selfcontained class instead of Sprite pointers
    std::vector<std::unique_ptr<Sprite> > sprites;
    Vec<int32_t> position;
    int32_t color;
    std::vector<uint32_t> tool_text;
    std::vector<uint32_t> tool_word_size;
    std::vector<std::vector<int32_t> > to_align;
    std::vector<int32_t> to_align_acc_width;
};

#endif
