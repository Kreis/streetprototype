#include "Scene.h"

Scene::Scene() {}
Scene::~Scene() {}

void Scene::init() {
  // currentMap = std::make_unique<MapStreet>();

  maps["data/Map/MapMenu.json"] = std::make_unique<MapMenu>();
  maps["data/Map/Street.json"] = std::make_unique<MapStreet>();
  maps["data/Map/Room.json"] = std::make_unique<MapRoom>();
  currentMap = maps["data/Map/MapMenu.json"].get();

  player = std::make_unique<Player>();
  player->init();

  currentMap->init(player.get());
}

void Scene::update(float deltatime) {

  // verify change map
  if (currentMap->nextMapName.length() > 0 &&
    currentMap->nextMapName != currentMap->name) {

    currentMap->off(player.get());
    if (maps[ currentMap->nextMapName ] == nullptr) {
      std::cout << "ERROR: Next map does not exists" << std::endl;
    }

    currentMap = maps[ currentMap->nextMapName ].get();
    currentMap->init(player.get());
  }

  currentMap->update(deltatime, player.get());

}

void Scene::draw() {
  currentMap->draw(player.get());
}






