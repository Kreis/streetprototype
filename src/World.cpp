#include "World.h"

World::World() {}
World::~World() {}

void World::start() {

  std::cout << "World start" << std::endl;

  Vec<int32_t> screenSize;
  screenSize.x = 1200;
  screenSize.y = 760;
  window.start("Street Prototype", screenSize.x, screenSize.y);
  Camera::get()->bounds.w = screenSize.x;
  Camera::get()->bounds.h = screenSize.y;

  Resources::get()->loadTextureAtlas("data/Textures/GlobalTexture.json");

  // Add All the texture will be used in the game
  std::vector<std::string> texturePriority;
  texturePriority.push_back("SHADERCLOUDS");
  texturePriority.push_back("data/Textures/Tiles.png");
  texturePriority.push_back("data/Textures/exit.png");
  texturePriority.push_back("data/Textures/exitOver.png");
  texturePriority.push_back("data/Textures/start.png");
  texturePriority.push_back("data/Textures/startOver.png");
  texturePriority.push_back("data/Textures/GlobalTexture.png");
  texturePriority.push_back("data/Fonts/arial25.png");

  SpriteBatch::get()->init(texturePriority);
  SpriteBatch::get()->setNullTextureVecVertex("SHADERCLOUDS");
  SpriteBatch::get()->setCameraFreeVecVertex("SHADERCLOUDS", true);

  Scene scene;
  scene.init();

  sf::Clock clock;
  float frameRateTime = 0;
  int32_t frameRate = 0;
  std::cout << "start loop" << std::endl;
  while (window.isOpen()) {

    window.update();
    if (InputState::get()->isTyped(KeyButton::W)) {
      window.close();
      break;
    }

    if (GlobalConfig::get()->exitGame) {
      std::cout << "exit game" << std::endl;
      window.close();
      break;
    }
  
    float elapsedTime = clock.getElapsedTime().asSeconds();
    clock.restart();
    scene.update(elapsedTime);

    scene.draw();
    SpriteBatch::get()->flush(&window);

    frameRate++;
    frameRateTime += elapsedTime;

    if (frameRateTime >= 1.0) {
      std::cout << "FPS: " << frameRate << std::endl;
      frameRate = 0;
      frameRateTime = 0;
    }
  }
  

}

