#include "GlobalConfig.h"

GlobalConfig::GlobalConfig() {}
GlobalConfig::~GlobalConfig() {}

std::unique_ptr<GlobalConfig> GlobalConfig::instancePtr = nullptr;

GlobalConfig* GlobalConfig::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<GlobalConfig>(new GlobalConfig()); 
  }

  return instancePtr.get();
}

