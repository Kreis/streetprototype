#ifndef _MAPGENERIC_H_
#define _MAPGENERIC_H_

#include <iostream>

class Player;
class MapGeneric {
public:
  virtual void init(Player* player) = 0;
  virtual void update(float deltatime, Player* player) = 0;
  virtual void draw(Player* player) = 0;
  virtual void off(Player* player) = 0;

  std::string name;
  std::string nextMapName = "";
};

#endif
