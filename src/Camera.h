#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <memory>

#include "sharedtypes.h"

class Camera {
public:
  ~Camera();

  static Camera* get();

  Rect<float> bounds;

private:
  Camera();
  static std::unique_ptr<Camera> instancePtr;
};

#endif
