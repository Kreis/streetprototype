#include "Sprite.h"

Sprite::Sprite() {
  horizontalSwitch = false;
  verticalSwitch = false;

  // TODO equivalent to r g b a
  color.x = 255;
  color.y = 255;
  color.w = 255;
  color.h = 255;

}
Sprite::~Sprite() {}


void Sprite::init(
  std::string _textureName,
  Rect<int32_t> _textureRect) {

  textureName = _textureName;
  textureRect = _textureRect; 

  bounds.w = textureRect.w;
  bounds.h = textureRect.h;
}

