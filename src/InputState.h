#ifndef _INPUTSTATE_H_
#define _INPUTSTATE_H_

#include <SFML/Graphics.hpp>

#include <vector>
#include <array>
#include <functional>
#include <iostream>

#include "sharedtypes.h"
#include "InputDataType.h"

class InputState
{
friend class Window;
public:
	~InputState();

	bool isPressed(KeyButton key) const;
	bool isTyped(KeyButton key) const;
	bool isReleased(KeyButton key) const;

	bool isResized() const;

	bool isPressed(MouseButton mouse_button) const;
	bool isTyped(MouseButton mouse_button) const;
	bool isReleased(MouseButton mouse_button) const;

	uint32_t getTextEntered() const;

	Vec<int32_t> getMousePosition();

	static InputState* get();

	bool closed;

private:
	InputState();
	void updateState(sf::RenderWindow* sfRenderWindow);
	static InputState* instancePtr;
	sf::Event ev;
	std::vector<uint8_t> vecKeyReleased;
	std::vector<uint8_t> vecMouseReleased;
	Vec<int32_t> mousePosition;
	bool resized;
	uint32_t textEntered;
	std::array<uint8_t, sf::Keyboard::Key::KeyCount> bufferKeys;
	std::array<uint8_t, sf::Mouse::Button::ButtonCount> bufferMouseButtons;
};

#endif
