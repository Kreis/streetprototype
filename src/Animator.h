#ifndef _ANIMATOR_H_
#define _ANIMATOR_H_

#include <iostream>
#include <map>

#include "sharedtypes.h"
#include "Sprite.h"
#include "TextureAtlas.h"
#include "SpriteBatch.h"

class Animator {
friend class Resources;
public:
  ~Animator();

  void runAnimation(std::string animationName);
  void update(float deltatime);
  void draw();

  std::string textureName;
  Rect<int32_t> bounds;
  bool loop = false;
  std::string currentAnimation = "";
  int frameId = 0;

  void setHorizontalSwitch(bool v);
  void setVerticalSwitch(bool v);

private:
  Animator();
  Sprite sprite;

  std::map<std::string, std::vector<Rect<int32_t> > > animations;
  std::map<std::string, std::vector<float> > delays;
  std::map<std::string, std::vector<Rect<int32_t> > > animationsColor;
  float currentTime = 0;
};


#endif
