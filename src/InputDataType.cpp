#include "InputDataType.h"

InputDataType::InputDataType() {}

sf::Keyboard::Key InputDataType::getSfmlKey(KeyButton key) {
  if (key == KeyButton::A) {
    return sf::Keyboard::Key::A;
  } else if (key == KeyButton::B) {
    return sf::Keyboard::Key::B;
  } else if (key == KeyButton::C) {
    return sf::Keyboard::Key::C;
  } else if (key == KeyButton::D) {
    return sf::Keyboard::Key::D;
  } else if (key == KeyButton::E) {
    return sf::Keyboard::Key::E;
  } else if (key == KeyButton::F) {
    return sf::Keyboard::Key::F;
  } else if (key == KeyButton::G) {
    return sf::Keyboard::Key::G;
  } else if (key == KeyButton::H) {
    return sf::Keyboard::Key::H;
  } else if (key == KeyButton::I) {
    return sf::Keyboard::Key::I;
  } else if (key == KeyButton::J) {
    return sf::Keyboard::Key::J;
  } else if (key == KeyButton::K) {
    return sf::Keyboard::Key::K;
  } else if (key == KeyButton::L) {
    return sf::Keyboard::Key::L;
  } else if (key == KeyButton::M) {
    return sf::Keyboard::Key::M;
  } else if (key == KeyButton::N) {
    return sf::Keyboard::Key::N;
  } else if (key == KeyButton::O) {
    return sf::Keyboard::Key::O;
  } else if (key == KeyButton::P) {
    return sf::Keyboard::Key::P;
  } else if (key == KeyButton::Q) {
    return sf::Keyboard::Key::Q;
  } else if (key == KeyButton::R) {
    return sf::Keyboard::Key::R;
  } else if (key == KeyButton::S) {
    return sf::Keyboard::Key::S;
  } else if (key == KeyButton::T) {
    return sf::Keyboard::Key::T;
  } else if (key == KeyButton::U) {
    return sf::Keyboard::Key::U;
  } else if (key == KeyButton::V) {
    return sf::Keyboard::Key::V;
  } else if (key == KeyButton::W) {
    return sf::Keyboard::Key::W;
  } else if (key == KeyButton::X) {
    return sf::Keyboard::Key::X;
  } else if (key == KeyButton::Y) {
    return sf::Keyboard::Key::Y;
  } else if (key == KeyButton::Z) {
    return sf::Keyboard::Key::Z;
  } else if (key == KeyButton::Num0) {
    return sf::Keyboard::Key::Num0;
  } else if (key == KeyButton::Num1) {
    return sf::Keyboard::Key::Num1;
  } else if (key == KeyButton::Num2) {
    return sf::Keyboard::Key::Num2;
  } else if (key == KeyButton::Num3) {
    return sf::Keyboard::Key::Num3;
  } else if (key == KeyButton::Num4) {
    return sf::Keyboard::Key::Num4;
  } else if (key == KeyButton::Num5) {
    return sf::Keyboard::Key::Num5;
  } else if (key == KeyButton::Num6) {
    return sf::Keyboard::Key::Num6;
  } else if (key == KeyButton::Num7) {
    return sf::Keyboard::Key::Num7;
  } else if (key == KeyButton::Num8) {
    return sf::Keyboard::Key::Num8;
  } else if (key == KeyButton::Num9) {
    return sf::Keyboard::Key::Num9;
  } else if (key == KeyButton::Escape) {
    return sf::Keyboard::Key::Escape;
  } else if (key == KeyButton::LControl) {
    return sf::Keyboard::Key::LControl;
  } else if (key == KeyButton::LShift) {
    return sf::Keyboard::Key::LShift;
  } else if (key == KeyButton::LAlt) {
    return sf::Keyboard::Key::LAlt;
  } else if (key == KeyButton::LSystem) {
    return sf::Keyboard::Key::LSystem;
  } else if (key == KeyButton::RControl) {
    return sf::Keyboard::Key::RControl;
  } else if (key == KeyButton::RShift) {
    return sf::Keyboard::Key::RShift;
  } else if (key == KeyButton::RAlt) {
    return sf::Keyboard::Key::RAlt;
  } else if (key == KeyButton::RSystem) {
    return sf::Keyboard::Key::RSystem;
  } else if (key == KeyButton::Menu) {
    return sf::Keyboard::Key::Menu;
  } else if (key == KeyButton::LBracket) {
    return sf::Keyboard::Key::LBracket;
  } else if (key == KeyButton::RBracket) {
    return sf::Keyboard::Key::RBracket;
  } else if (key == KeyButton::Comma) {
    return sf::Keyboard::Key::Comma;
  } else if (key == KeyButton::Period) {
    return sf::Keyboard::Key::Period;
  } else if (key == KeyButton::Quote) {
    return sf::Keyboard::Key::Quote;
  } else if (key == KeyButton::Slash) {
    return sf::Keyboard::Key::Slash;
  } else if (key == KeyButton::Tilde) {
    return sf::Keyboard::Key::Tilde;
  } else if (key == KeyButton::Equal) {
    return sf::Keyboard::Key::Equal;
  } else if (key == KeyButton::Dash) {
    return sf::Keyboard::Key::Dash;
  } else if (key == KeyButton::Space) {
    return sf::Keyboard::Key::Space;
  } else if (key == KeyButton::Enter) {
    return sf::Keyboard::Key::Enter;
  } else if (key == KeyButton::Tab) {
    return sf::Keyboard::Key::Tab;
  } else if (key == KeyButton::PageUp) {
    return sf::Keyboard::Key::PageUp;
  } else if (key == KeyButton::PageDown) {
    return sf::Keyboard::Key::PageDown;
  } else if (key == KeyButton::End) {
    return sf::Keyboard::Key::End;
  } else if (key == KeyButton::Home) {
    return sf::Keyboard::Key::Home;
  } else if (key == KeyButton::Insert) {
    return sf::Keyboard::Key::Insert;
  } else if (key == KeyButton::Delete) {
    return sf::Keyboard::Key::Delete;
  } else if (key == KeyButton::Add) {
    return sf::Keyboard::Key::Add;
  } else if (key == KeyButton::Subtract) {
    return sf::Keyboard::Key::Subtract;
  } else if (key == KeyButton::Multiply) {
    return sf::Keyboard::Key::Multiply;
  } else if (key == KeyButton::Divide) {
    return sf::Keyboard::Key::Divide;
  } else if (key == KeyButton::Left) {
    return sf::Keyboard::Key::Left;
  } else if (key == KeyButton::Right) {
    return sf::Keyboard::Key::Right;
  } else if (key == KeyButton::Up) {
    return sf::Keyboard::Key::Up;
  } else if (key == KeyButton::Down) {
    return sf::Keyboard::Key::Down;
  } else if (key == KeyButton::Numpad0) {
    return sf::Keyboard::Key::Numpad0;
  } else if (key == KeyButton::Numpad1) {
    return sf::Keyboard::Key::Numpad1;
  } else if (key == KeyButton::Numpad2) {
    return sf::Keyboard::Key::Numpad2;
  } else if (key == KeyButton::Numpad3) {
    return sf::Keyboard::Key::Numpad3;
  } else if (key == KeyButton::Numpad4) {
    return sf::Keyboard::Key::Numpad4;
  } else if (key == KeyButton::Numpad5) {
    return sf::Keyboard::Key::Numpad5;
  } else if (key == KeyButton::Numpad6) {
    return sf::Keyboard::Key::Numpad6;
  } else if (key == KeyButton::Numpad7) {
    return sf::Keyboard::Key::Numpad7;
  } else if (key == KeyButton::Numpad8) {
    return sf::Keyboard::Key::Numpad8;
  } else if (key == KeyButton::Numpad9) {
    return sf::Keyboard::Key::Numpad9;
  } else if (key == KeyButton::F1) {
    return sf::Keyboard::Key::F1;
  } else if (key == KeyButton::F2) {
    return sf::Keyboard::Key::F2;
  } else if (key == KeyButton::F3) {
    return sf::Keyboard::Key::F3;
  } else if (key == KeyButton::F4) {
    return sf::Keyboard::Key::F4;
  } else if (key == KeyButton::F5) {
    return sf::Keyboard::Key::F5;
  } else if (key == KeyButton::F6) {
    return sf::Keyboard::Key::F6;
  } else if (key == KeyButton::F7) {
    return sf::Keyboard::Key::F7;
  } else if (key == KeyButton::F8) {
    return sf::Keyboard::Key::F8;
  } else if (key == KeyButton::F9) {
    return sf::Keyboard::Key::F9;
  } else if (key == KeyButton::F10) {
    return sf::Keyboard::Key::F10;
  } else if (key == KeyButton::F11) {
    return sf::Keyboard::Key::F11;
  } else if (key == KeyButton::F12) {
    return sf::Keyboard::Key::F12;
  } else if (key == KeyButton::F13) {
    return sf::Keyboard::Key::F13;
  } else if (key == KeyButton::F14) {
    return sf::Keyboard::Key::F14;
  } else if (key == KeyButton::F15) {
    return sf::Keyboard::Key::F15;
  } else if (key == KeyButton::Pause) {
    return sf::Keyboard::Key::Pause;
  } else if (key == KeyButton::KeyCount) {
    return sf::Keyboard::Key::KeyCount;
  } else if (key == KeyButton::BackSpace) {
    return sf::Keyboard::Key::BackSpace;
  } else if (key == KeyButton::BackSlash) {
    return sf::Keyboard::Key::BackSlash;
  } else if (key == KeyButton::SemiColon) {
    return sf::Keyboard::Key::SemiColon;
  }

  return sf::Keyboard::Key::Unknown;
}

sf::Mouse::Button InputDataType::getSfmlMouseButton(
    MouseButton mouseButton) {
  if (mouseButton == MouseButton::Left) {
    return sf::Mouse::Button::Left;
  } else if (mouseButton == MouseButton::Right) {
    return sf::Mouse::Button::Right;
  } else if (mouseButton == MouseButton::Middle) {
    return sf::Mouse::Button::Middle;
  } else if (mouseButton == MouseButton::XButton1) {
    return sf::Mouse::Button::XButton1;
  } else if (mouseButton == MouseButton::XButton2) {
    return sf::Mouse::Button::XButton2;
  }

  return sf::Mouse::ButtonCount;
}

