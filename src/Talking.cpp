#include "Talking.h"

Talking::Talking() {}
Talking::~Talking() {}

void Talking::init(Rect<int32_t> _dialogRect) {
  std::cout << "init talking" << std::endl;

  // Text area
  Resources::get()->loadFontConfig("data/Fonts/arial25.xml");
  FontConfig* fc = Resources::get()->getFontConfig("data/Fonts/arial25.xml");

  // adjust display size
  dialogRect = _dialogRect;
  displayRect.x = _dialogRect.x;
  displayRect.y = _dialogRect.y;
  int32_t offset = 32;
  displayRect.w = 160;
  displayRect.h = 160;
  dialogRect.x += 160 + offset;
  dialogRect.w -= 160 + offset;
  textArea = TextArea::create(*fc, dialogRect);

  // displays
  Resources::get()->loadAnimator(animDisplay01);
  Resources::get()->loadAnimator(animDisplay02);

  Animator* a1 = Resources::get()->getAnimator(animDisplay01);
  Animator* a2 = Resources::get()->getAnimator(animDisplay02);

  a1->bounds.x = a2->bounds.x = displayRect.x;
  a1->bounds.y = a2->bounds.y = displayRect.y;

  a1->runAnimation("idle");
  a2->runAnimation("idle");
}

void Talking::update(float deltatime, int32_t tempo) {
  Animator* a1 = Resources::get()->getAnimator(animDisplay01);
  Animator* a2 = Resources::get()->getAnimator(animDisplay02);
 
  if (talkTempo != tempo) {
    runningTime = 0;
  }
  runningTime += deltatime;
  talkTempo = tempo;

  // calculate number of letters for this time
  uint32_t num = (uint32_t)(runningTime * 20);

  if (talkTempo == 1) {
    a1->update(deltatime);
    std::wstring text = L"hello, I'm happy to see you here";
    if (num > text.length()) num = text.length();
    textArea->setText(text.substr(0, num), 255);
  }
  if (talkTempo == 2) {
    a2->update(deltatime);
    std::wstring text = L"are you going to stay for a while?";
    if (num > text.length()) num = text.length();
    textArea->setText(text.substr(0, num), 255);
  }
  if (talkTempo == 3) {
    a2->update(deltatime);
    std::wstring text = L"that's great";
    if (num > text.length()) num = text.length();
    textArea->setText(text.substr(0, num), 255);
  }
  if (talkTempo == 4) {
    a1->update(deltatime);
    std::wstring text = L"relaxing time";
    if (num > text.length()) num = text.length();
    textArea->setText(text.substr(0, num), 255);
  }
}

void Talking::draw() {
  Animator* a1 = Resources::get()->getAnimator(animDisplay01);
  Animator* a2 = Resources::get()->getAnimator(animDisplay02);

  if (talkTempo == 1) {
    a1->draw();    
  }
  if (talkTempo == 2) {
    a2->draw();    
  }
  if (talkTempo == 3) {
    a2->draw();    
  }
  if (talkTempo == 4) {
    a1->draw();    
  }

  textArea->draw();
}









