#ifndef _MAPMENU_H_
#define _MAPMENU_H_

#include <iostream>

#include "MapGeneric.h"
#include "Resources.h"
#include "Camera.h"
#include "GlobalConfig.h"
#include "sharedtypes.h"
#include "InputState.h"

class MapMenu : public MapGeneric {
public:
  MapMenu();
  ~MapMenu();

  void init(Player* player) override;
  void update(float deltatime, Player* player) override;
  void draw(Player* player) override;
  void off(Player* player) override;

  Rect<int32_t> startRect;
  Rect<int32_t> exitRect;

  Sprite startSprite;
  Sprite exitSprite;

  std::string exitName;
  std::string exitOverName;

  std::string startName;
  std::string startOverName;

  // 1 = start, 2 = exit
  int32_t selectedButton;
};

#endif
