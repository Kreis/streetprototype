#include "MapStreet.h"

MapStreet::MapStreet() {}
MapStreet::~MapStreet() {}

void MapStreet::init(Player* player) {

  name = "data/Map/Street.json";
  nextMapName = name;

  std::cout << "init map " << name << std::endl;

  Resources::get()->loadMap(name);
  Map* map = Resources::get()->getMap(name);

  SpriteBatch::get()->fixVecVertex(
    map->tilesetTextureName,
    map->getVecVertex());

  Rect<int32_t> doorRect;
  for (int32_t i = 0; i < map->objectsName.size(); i++) {
    std::cout << "iterate object "
      << map->objectsName[ i ] << std::endl;
    if (map->objectsName[ i ] == "physics") {
      std::cout << "is physics" << std::endl;
      int32_t ptmp = Physics::get()->generateBody(
        map->objectsRect[ i ].x,
        map->objectsRect[ i ].y,
        map->objectsRect[ i ].w,
        map->objectsRect[ i ].h);
      bodies.push_back(ptmp);
    }

    if (map->objectsName[ i ] == "start_spawn") {
      Vec<int32_t> v;
      v.x = map->objectsRect[ i ].x;
      v.y = map->objectsRect[ i ].y;
      player->setPosition(v);
      // set camera in the middle of player
      Camera::get()->bounds.x =
        v.x - (Camera::get()->bounds.w / 2.0) + (player->getBounds().w / 2.0);
    }

    if (map->objectsName[ i ] == "door") {
      doorRect = map->objectsRect[ i ];
    }
  }


  // animator arrow
  Resources::get()->loadAnimator("data/Textures/arrow.json");
  Animator* animator = Resources::get()->getAnimator("data/Textures/arrow.json");

  animator->runAnimation("moving");
  animator->bounds.x = doorRect.x - 32;
  animator->bounds.y = doorRect.y - 128;

  // clouds
  clouds.init();

  // music
  Resources::get()->loadMusic("data/Sound/StreetBackground.ogg");
  sf::Music* music =
    Resources::get()->getMusic("data/Sound/StreetBackground.ogg");
  music->play();
  int32_t volume = GlobalConfig::get()->volume;
  music->setVolume((float)(volume * volume) / 100.0);
  music->setLoop(true);

  // sounds
  Resources::get()->loadSound("data/Sound/door.wav");

  // camera fix reduction in 200 height in screen
  Camera::get()->bounds.y = 200;

  // leafs
  Resources::get()->loadAnimator("data/Textures/leafs.json");
  Animator* leafs = Resources::get()->getAnimator("data/Textures/leafs.json");
  leafs->runAnimation("moving");
  leafs->bounds.x = 150;
  leafs->bounds.y = 300;

}

void MapStreet::update(float deltatime, Player* player) {
  player->update(deltatime);

  Physics::get()->update();
  
  Rect<int32_t> playerBounds = player->getBounds();
  
  Rect<float> cameraBounds = Camera::get()->bounds;
  float chunk = cameraBounds.w / 5.0;
  
  // going too right
  if (playerBounds.x + playerBounds.w > chunk * 3 + cameraBounds.x) {
    Camera::get()->bounds.x =
      playerBounds.x + playerBounds.w - (chunk * 3);
  }
  // going too left
  if (playerBounds.x < chunk * 2 + cameraBounds.x) {
    Camera::get()->bounds.x =
      playerBounds.x - (chunk * 2);
  }
  // if hit limits
  if (Camera::get()->bounds.x < 0) {
    Camera::get()->bounds.x = 0;
  }
  Map* map = Resources::get()->getMap(name);
  int32_t lengthMap = map->width * map->tilewidth;
  if (Camera::get()->bounds.x + Camera::get()->bounds.w > lengthMap) {
    Camera::get()->bounds.x = lengthMap - Camera::get()->bounds.w;
  }

  Animator* arrow = Resources::get()->getAnimator("data/Textures/arrow.json");
  arrow->update(deltatime);
  
  float volume = GlobalConfig::get()->volume;

  // door
  if (InputState::get()->isTyped(KeyButton::Up) && actionableDoor) {
    nextMapName = "data/Map/Room.json";
    Resources::get()->getSound("data/Sound/door.wav")->setVolume(
      (float)(volume * volume) / 100.0 * 1.2);
    Resources::get()->getSound("data/Sound/door.wav")->play();
  }

  Rect<int32_t> prect = player->getBounds();
  actionableDoor = prect.x + prect.w > arrow->bounds.x + 32 &&
    prect.x < arrow->bounds.x + arrow->bounds.w - 32;

  // clouds
  clouds.update(deltatime);

  
  Animator* leafs = Resources::get()->getAnimator("data/Textures/leafs.json");
  leafs->update(deltatime);

  // volume
  if (InputState::get()->isTyped(KeyButton::Add)) {
    GlobalConfig::get()->volume += 10;
    if (GlobalConfig::get()->volume > 100) GlobalConfig::get()->volume = 100;
    volume = GlobalConfig::get()->volume;

    Resources::get()->getMusic("data/Sound/StreetBackground.ogg")->
      setVolume((float)(volume * volume) / 100.0);
    std::cout << "set volume as " << GlobalConfig::get()->volume << std::endl;
  }
  if (InputState::get()->isTyped(KeyButton::Subtract)) {
    GlobalConfig::get()->volume -= 10;
    if (GlobalConfig::get()->volume < 0) GlobalConfig::get()->volume = 0;
    volume = GlobalConfig::get()->volume;

    Resources::get()->getMusic("data/Sound/StreetBackground.ogg")->
      setVolume((float)(volume * volume) / 100.0);
    std::cout << "set volume as " << GlobalConfig::get()->volume << std::endl;
  }
}

void MapStreet::draw(Player* player) {

  player->draw();

  Animator* arrow = Resources::get()->getAnimator("data/Textures/arrow.json");

  if (actionableDoor) {
    arrow->draw();
  }

  clouds.draw();

  
  Animator* leafs = Resources::get()->getAnimator("data/Textures/leafs.json");
  leafs->draw();
}

void MapStreet::off(Player* player) {
  std::cout << "off map street" << std::endl;
  SpriteBatch::get()->clearVecVertex("data/Textures/Tiles.png");

  for (int32_t i : bodies) {
    Physics::get()->getBody(i)->off();
  }


  Resources::get()->getMusic("data/Sound/StreetBackground.ogg")->stop();
}



