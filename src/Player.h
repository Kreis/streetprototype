#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <iostream>

#include "Animator.h"
#include "Resources.h"
#include "Physics.h"
#include "Camera.h"
#include "Parabola.h"

class Player {
public:
  Player();
  ~Player();

  void init();
  void update(float deltatime);
  void draw();

  Rect<int32_t> getBounds();
  void setPosition(Vec<int32_t> pos);

  uint32_t bodyId;

  Parabola parabola;
  bool jumping;
};


#endif
