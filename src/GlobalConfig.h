#ifndef _GLOBAL_CONFIG_H_
#define _GLOBAL_CONFIG_H_

#include <memory>

class GlobalConfig {
public:
  ~GlobalConfig();

  static GlobalConfig* get();

  bool exitGame = false;
  int32_t volume = 40;

private:
  GlobalConfig();
  static std::unique_ptr<GlobalConfig> instancePtr;
};



#endif
