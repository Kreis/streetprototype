#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform float u_time;

float random(in vec2 uv) {
    return fract(sin(dot(floor(uv), vec2(332.11, 533.52))) * 3352.32);
}

float noise(in vec2 uv) {

    vec2 i = floor(uv);
    vec2 f = fract(uv);

    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));
    
    vec2 u = f * f * (3.0 - 2.0 * f);
    
    return mix(a, b, u.x) +
        (c - a) * u.y * (1.0 - u.x) +
        (d - b) * u.x * u.y;

}

float fbm(in vec2 uv) {

    const int octaves = 4;
    float gain = 0.5;
    float lacunarity = 2.0;
    
    float frequency = 2.0;
    float amplitude = 0.5;
    float value = 0.0;
    
    for (int i = 0; i < octaves; i++) {
    
        value += amplitude * noise(uv * frequency);
        frequency *= lacunarity;
        amplitude *= gain;
    
    }

    return value;
}

void main()
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = gl_TexCoord[0].xy/u_resolution.xy;

    vec2 r = vec2(0.0);
    r.x = fbm(uv * 7.21 + 0.05 * u_time);
    r.y = fbm(uv * 4.08 + 0.03 * u_time);
    float f = fbm(uv * 2.0 + r);

    // option 2
    vec3 cola = vec3(0.1442, 0.1224, 0.63523);
    vec3 colb = vec3(0.0511, 0.4111, 0.97585);
    vec3 col = mix(cola, colb, clamp(f*f*1.5, 0.0, 1.0));

    // Output to screen
    gl_FragColor = vec4(col * (f*f*f + 0.75*f*f + 0.5*f), 1.0);
}
