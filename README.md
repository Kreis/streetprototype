## Street prototype

Game 2D Platform prototype of chill experience inspired in lofi music videos


### Commands to run compile it using Cmake
Require download SFML library and install it in `C:/3rdParty/SFML`

```
mkdir build 
cd build

cmake -G "Visual Studio 16 2019" ..

cmake --build . --config Debug

Debug/streetprototype.exe
```

